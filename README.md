# GNU/LINUX

Linux est un système d'exploitation, un programme qui permet le pilotage et l’interaction de l'ensemble des périphériques reliés entre eux par la carte mère.

On parle souvent du noyau Linux. Le noyau d'un système d'exploitation est la base, qui permet de manipuler l'ensemble des périphériques mais n'effectue aucune opération. Les appels système permettent des interactions entre les programmes et le noyau. De ce fait, lorsqu'on développe un programme pour Linux, nous n'avons pas a nous soucier de comment les données peuvent être écrites. On indique simplement qu'on souhaite écrire un fichier, et le noyau va s'occuper du reste. C'est une abstraction forte pour le confort du développeur.

Quand nous évoquons Linux aujourd'hui, il ne s'agit généralement plus du noyau ( ou kernel ), mais tout un ensemble de logiciels qui permette un usage de l'informatique tel qu'on le connais. On peut parler de cet ensemble sous le nom GNU/Linux . Hérité de la logique d' UNIX, une partie de la philosophie est d'avoir des logiciels qui font une tâche simple, mais bien. On se retrouve vite avec un panel d'outils nous permettant de travailler sereinement.

Le projet GNU a pour but de proposer un contrôle complet de l'ordinateur et d'accorder une grande liberté que ce soit l'utilisation, la conception ou la distribution de logiciels. GNU peut être considéré comme une suite de logiciels autour du système d'exploitation et une alternative à Unix et ses verrous propriétaires. C'est ainsi que le noyau Linux, entouré d'une grande partie d'outils de GNU, donne GNU/Linux ( communément appelé simplement Linux ) . Il existe dans de nombreuses distributions ( Debian, Ubuntu, RHEL, centos, arch ) qui disposent chacune d'architectures et de méthodes de fonctionnement différents, tout en étant construites sur de même bases.

Les applications développées sous Linux, de par une organisation commune et l'abstraction des appels système sont réutilisables sur des systèmes pouvant être très différents.

Linux est un système multi-utilisateurs. C'est à dire qu'il permet à de nombreux utilisateurs de travailler en même temps. L' ouverture, la liberté et ses fonctionnements de base font que Linux est massivement utilisé pour les serveurs ,les clusters de calculs et les rouages du cloud mais aussi comme système embarqué et machine personnelle (bureautique, etc.. ).

Souvent, pour une tâche précise, l'écosystème est tel qu'on a le choix entre différents logiciels, que l'on peux intégrer dans son environnement de travail, et en tant que développeur on peux améliorer ou reprendre une base d'un projet existant pour l'adapter à sa guise.

# SHELL

Maintenant que nous avons parcouru ce qu'est Linux sur un plan théorique, nous allons nous aventurer dans le SHELL, autrement dit l'utilisation de Linux

Le shell est une interface utilisateur purement textuelle et qui s'utilise uniquement au clavier. On utilise aussi l'acronyme CLI ( Command Line Interfalce) à l'opposé de GUI ( Graphical User Interface ), qui repose sur une interface fenêtrée avec le moyen d'une souris.

Dans un environnement graphique ( sur un P.C par exemple ), on ouvre un terminal pour avoir une fenêtre contenant un shell.

Cet interface permet d'invoquer le logiciel à utiliser, et adapter son comportement sous la forme d'instruction. plutôt que d'ouvrir un navigateur de fichier (GUI) , aller cliquer dans chaque répertoire, pour enfin double cliquer sur le fichier fasta voulu afin de l'ouvrir, on peux le faire en shell de cette manière :

`more /home/afalce/build/maker/MWAS/data/pyu-protein.fasta`

et le contenu du fichier s'affiche dans le terminal. Dans cet exemple, on utilise more, un logiciel permettant de consulter le contenu d'un long fichier dans la taille précise du terminal, et de naviguer dedans.

Chaque logiciel dispose de son manuel, que l'on peut invoquer en faisant :

`man commande`

Pour l'exemple de more, cela affichera son manuel

```

MORE(1) User Commands MORE(1)

NAME

   more - file perusal filter for crt viewing


SYNOPSIS

   more [options] file...


DESCRIPTION

   more  is a filter for paging through text one screenful at a time.  This version is espe‐

   cially primitive.  Users should realize that less(1) provides more(1) emulation plus  ex‐

   tensive enhancements.


OPTIONS

   Options are also taken from the environment variable MORE (make sure to precede them with

   a dash (-)) but command-line options will override those.

   -d, --silent

          Prompt with "[Press space to continue, 'q' to quit.]", and display "[Press 'h' for

          instructions.]" instead of ringing the bell when an illegal key is pressed.

   -l, --logical

          Do not pause after any line containing a ^L (form feed).

   -f, --no-pause

          Count logical lines, rather than screen lines (i.e., long lines are not folded).


[...]

```

On y lit la description du logiciel, son utilisation et des détails sur son usage. On voit qu'on peux utiliser des options, par exemple en utilisant :

`more -d fichier`

on verrais afficher dans more que la barre d'espace permet de passer à la page suivante, et 'q' de quitter le logiciel.

## Entrée/Sorties standard

Une notion très importante du shell est celle des entrées et sorties standards. En shell, la sortie standard est l'affichage ( écrire du texte à l'écran ) et l'entrée standard est le clavier ( on "rentre" du texte ). Il existe aussi la sortie standard d'erreur, qui est par défaut l'affichage également.

La particularité du shell est que tout est une chaîne de caractère : on manipule du texte seulement. Il est possible d'orienter les sorties et l'entrée standard vers ou depuis des fichiers ( qui contiendraient du texte ). Un grand classique est l'utilisation du pipe ( | ) qui permet de combiner plusieurs outils en injectant la sortie standard de l'un vers l'entrée standard de l'autre. Ainsi on peux construire des pipelines.

Par exemple, je vai vous montrer à quoi sers head :
```
$ man head |head -n 5

HEAD(1) User Commands HEAD(1)

NAME

   head - output the first part of files
```

J'ai passé le résultat de man head (normalement affiché sur la sortie standard) dans l'entrée standarde de head, qui m'affiche uniquement les 5 premières lignes.

Passons aux choses intéressantes : voyons plutôt sur le fichier fasta comment compter le nombre de séquences :

Vu qu'on veux récupérer le nombre de séquence, on vas filtrer le contenu du fichier pour n'avoir que les lignes contenant un chevron :
```
$ grep ">" /home/afalce/build/maker/MWAS/data/pyu-protein.fasta

[...]

>sp|Q9WU83|DPM1_CRIGR

>sp|Q9XG77|PSA6_TOBAC

>sp|Q9ZSB5|UBP10_ARATH

On a en sortie la liste des séquences. Maintenant, on veux des nombres !

extrait du man de cat :

NAME
       cat - concatenate files and print on the standard output

SYNOPSIS
       cat [OPTION]... [FILE]...

DESCRIPTION
  -n, --number
              number all output lines

```
Vu qu'on a une ligne par séquence, on pourrait utiliser ça :
```
$ grep ">" pyu-protein.fasta  | cat -n
  [...]
  1303  >sp|Q9WU83|DPM1_CRIGR
  1304  >sp|Q9XG77|PSA6_TOBAC
  1305  >sp|Q9ZSB5|UBP10_ARATH
```

Mais tout ça fait beaucoup d'informations inutile dans notre petite fenêtre à mon goût.

```
WC(1)                                     User Commands                                    WC(1)

NAME
       wc - print newline, word, and byte counts for each file

SYNOPSIS
       wc [OPTION]... [FILE]...
       wc [OPTION]... --files0-from=F

DESCRIPTION
      [...]

       -l, --lines
              print the newline counts

```
wc avec l'option -l ( compter le nombre de retour à la ligne) me paraît beaucoup plus approprié :
```
$ grep ">" pyu-protein.fasta | wc -l
1305
```

On a exactement ce qu'on veux : le nombre de séquences. pas une information de plus, pas une de moins. en combinant deux outils qui font chacun une tache simple (filtrer et compter). Le tout réside dans la connaisance du panel d'outils à disposition, et la bonne lecture des manuels.


## Variables

Le shell se base aussi sur des variables d'environnement. Dans cette interface utilisateur, des variables peuvent être lues par les différents logiciels et permettent de transmettre des informations sur le SHELL. Ces variables influent aussi directement le SHELL.
L'ensemble des variables d'environnement peuvent être affichées avec la commande

`env`

Qui liste un grand nombre d'informations :

```

SHELL=/bin/bash

EDITOR=vim

PWD=/home/user

LOGNAME=user

HOME=/home/user

LANG=fr_FR.UTF-8

USER=user

PATH=/usr/local/bin:/usr/bin:/bin

```

La variable PWD indique le répertoire courant. PATH est très importante car elle indique où sont localisés les logiciels. Par exemple :

```

$ whereis cat

cat: /bin/cat /usr/share/man/man1/cat.1.gz

```

en tapant cat, le shell vas parcourir les différents répertoires indiqués par PATH avant de trouver le binaire en question.

# Système de fichiers:

Nous avons vu dans les différents exemples beaucoup de chemin de fichiers ( ou répertoires ). Sous les systèmes d'exploitations de la famille UNIX, tout commence à la racine, appellée souvent rootfs : "/". C'est le point de départ de l'arborescence. Chaque fichier accessible peut être indiqué en utilisant "/" en premier charactère. On appelle cela un chemin absolu. Peu importe le répertoire dans lequel on se trouve, en indiquant le chemin absolu ( en partant de la racine donc ), on peut accéder au fichier.

Il y a toute une structure "fixe" sous linux nommée FHS pour Filesystem Hierarchy Standard, https://fr.wikipedia.org/wiki/Filesystem_Hierarchy_Standard :

`$ ls /`

* bin : les fichiers binaires ( executables ) de base, les outils UNIX/POSIX
* boot : les fichiers du gestionnaire de démarrage ( bootloader )
* dev : les fichiers représentant les périphériques ( devices )
* etc : les fichiers de configuration du système (
* home : les dossiers spécifiques aux utilisateurs
* lib : les librairies partagés ainsi que les modules du noyau
* lost+found : forcément présent sur une partition de type ext4 ( plus de détails plus tard )
* media : point de montage des périphériques de stockage amovibles
* mnt : point de montage temporaire ( souvent lié à l'administration )
* opt : chemin d'installation pour des applications spécifiques
* proc : représentation du noyau en mode fichier
* root : équivalent du home réservé à l'administrateur
* run : fichiers résidents en mémoire vive
* sbin : les binaires réservés à l'administrateur
* srv : données liés aux applications ( services ) du serveur
* sys : semblable à /proc, mais plus lié au matériel
* tmp : fichiers temporaires
* usr : arborescence semblable à / pour les outils non essentiels à linux, souvent les fichiers de taille fixes ( binaires, code source )
* usr/bin : binaires installés par la distribution
* usr/local/bin : binaires installés manuellement ( compilation et make install )
* var : données variables, de taille grandissantes, souvent utilisé par les logiciels du système

Navigation dans l'arborescence

J'évoquait précédemment les chemins relatif. Par exemple si on est dans le chemin /usr , on peux lister le contenu de /usr/local/bin de la manière suivante :

`ls local/bin`

Attention surtout à ne pas indiquer le "/" au début, autrement c'est un chemin absolu ( /local/bin ) qui n'existe pas.

il y a deux notations importantes : "." pour le répertoire courant, et ".." pour le répertoire parent. Cette dernière permet de naviguer aisément dans l'arborescence. Par exemple pour retourner dans le home si on est l'utilisateur "user" :

`cd ../home/user`

A noter que vu qu'on passe par la racine, on peux indiquer le chemin absolu :

`cd /home/user`

Mais si vous voulez vraiment gagner du temps pour retrouver votre home, de n'importe où, vous pouvez indiquer :

`cd ~`
voir :
`cd`
qui sans paramètres vous ramène à la maison

Et si vous voulez afficher le contenu d'un des fichiers de user dans un répertoire précis :

`cat ~/repertoire/fichier`

# Multi-utilisateurs et privilèges 

Vu qu'un système linux est conçu pour acceuillir de nombreux utilisateurs, il y a une séparation nette entre l'administrateur ( root ), ses droits sur les fichiers ainsi que les programmes à éxecuter. On parle souvent de "privilèges".

Pour cette séparation, les utilisateurs existent dans un fichier qui sers comme une base de donnée : /etc/passwd. Les informations de chaque utilisateurs y sont décrites, comme le répertoire de l'utilisateur, par convention le nom de l'utilisateur dans le répertoire home. L'utilisateur, quand il se connecte, est dans son home ( exemple avec un utilisateur 'user' ) : /home/user . Dans bash, le home de l'utilisateur est aussi noté '~'. Ce qui compte vraiment pour le système, c'est l'identifiant d'utilisateur. L'utilisateur root à l'UID ( User Identifiant ) 0. Jusqu'à 999, ce sont généralement des comptes de "service", associés à des programmes par exemple, qui permet une séparation nette des privilèges.

Les utilisateurs humains dans cette convention commencent à partir de 1000. Pour chaque fichier, la propriété et les droits sont attribués : droits en lecture (r pour read), droit en écriture (w pour write)et droit en exécution (x pour execution). Un fichier est toujours associé à un UID ( utilisateur ) et un GID (groupe, défini dans /etc/group) propriétaire, et on as de défini un ensemble "rwx" pour l'utilisateur, le groupe, et tous les autres.

Çeci permet une gestion fine des droits d'accès, et dans le cas où différents utilisateurs sont amenés à administrer une machine, on passe par des comptes utilisateurs et le logiciel "sudo" pour super user do ( super-user étant l'autre nom de root )

# SSH

openSSH est un outil ayant pour fonctionnalité première l'ouverture d'un shell distant en passant par le réseau. Il s'avère être un vrai couteau suisse mais nous n'aborderons que ce premier aspect. Comme chaque outil réseau, il s'appuie sur le protocole standardisé SSH, et on l'invoque avec la commande "ssh". Il faut spécifier la machine à atteindre, par son IP ou son nom, et spécifier l'utilisateur ( sauf dans le cas où votre utilisateur local - sur votre machine - porte le même nom ). Pour ouvrir un shell sur la machine "hôte" avec l'utilisateur user, on utilise par exemple la commande :

`ssh user@hôte`

En fonction de la machine hôte, il peut être demandé le mot de passe du compte, ou l'utilisation de clés SSH ( authentification plus robuste et plus élaborée en terme de sécurité ). Les clés ssh fonctionnent par paire :

une clé privée, restant sur l'ordinateur et qu'il est nécessaire de bien protéger ( c'est vraiment la clé pour rentrer sur le serveur ), c'est pourquoi on utilise souvent une passphrase ( long mot de passe ) pour la déverouiller.

une clé publique, qui est plus de l'ordre du cadenas et que l'administrateur va déposer sur le serveur en l'associant à votre compte utilisateur, qui peut être communiqué plus aisément qu'un mot de passe
