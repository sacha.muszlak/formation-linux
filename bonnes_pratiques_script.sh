#!/bin/bash

# on error exit flag : set -e
set -o errexit 

# error if a var is unset : set -u
set -o nounset

# raise error in pipe
set -o pipefail

# date : 20160209
# version : 0.02
# authors : Nathalie Viera (CBGP INRA), Jean-François Martin (CBGP SupAgro) & Alex Dehne G (CBGP INRA)
# licence : GPL
#
########################
# Short description :
#    This script take the first and last n lines from a fastq file 
#    and write them to files in an output directory. 
#
########################
# USAGE :
#	  trimFastq.sh <fastq file>   <lines number>   <output directory>
#			#1 parameter = fastq file with .fastq extention
#			#2 parameter = number of lines to extract, should be a integer from 1 to 9
#                          this number must be
#			#3 parameter = output directory to store the result files. Would be created if it not exits
#		Example : bash trimFastq.sh brady_908.fastq   8   ./ResultsTrimmed/
#
########################
# Long Description :
#    This script has no biological meaning. It's an excerice for the CBGP Enlarge Your Linux course.
#    bla bla bla bla ... 
#    bla bla bla bla ...
#    bla bla bla bla ...  ouput file with .head and .tail extentions ... bla bla bla 
#    bla bla bla bla ...
#    bla bla bla bla ... 
#    bla bla bla bla ...
±±#
########################
# Known problems and caveats
#  - the script print to stdout the required fastq file number
#
########################
# DEVELOPPER'S TODO LIST
#  - redirect the stdout to /dev/null in order to avoid to print the required fastq file lines number
#    when the grep do not fail.
#    see the grep applied to $LINES_NUM" in the "PARAMETERS TESTS" section.
#
#




######################################
# SCRIPT PARAMETERS
SCRIPT_NAME=$(basename "$0")
INPUT_FILE="$1"
LINES_NUM="$2"
OUTPUT_DIR="$3"



######################################
# USEFUL SCRIPT VARIABLES
input_file_basename=""


####################################@
# FUNCTIONS

usage(){
	echo -e "$SCRIPT_NAME script :" 
	echo -e "\t\tTake the first and last n lines from a fastq file \n\t\tand write them to files in an output directory."
	echo -e "\t\tPlease read full information in the script first part.\n"	
	echo -e "$SCRIPT_NAME usage :"
	echo -e "\t\t$SCRIPT_NAME <fastq file>   <lines number>   <output directory>"
	echo -e "\t\t\t#1 parameter = fastq file with .fastq extention"
	echo -e "\t\t\t#2 parameter = number of lines to extract, should be a integer from 1 to 9"
	echo -e "\t\t\t#3 parameter = output directory to store the result files. Would be created if it not exits"
	echo -e "\t\tExample : bash $SCRIPT_NAME brady_908.fastq   8   ./ResultsTrimmed/\n"	
}


######################################
# PARAMETERS TESTS

(( "$#" == 3 )) || { (usage ; echo -e "ERROR : 3 parameters required but $# provided\n";) >&2 ; exit 2;}

# testing the first parameter : the fastq file
[ -e "$INPUT_FILE" ] || { (usage ; echo -e "ERROR : file not found error for $INPUT_FILE \n") >&2 ; exit 2;}
[ -f "$INPUT_FILE" ] || { (usage ; echo -e "ERROR : standard file expected for $INPUT_FILE \n") >&2; exit 2;}
[ -r "$INPUT_FILE" ] || { (usage ; echo -e "ERROR : Read permission error for $INPUT_FILE \n") >&2 ; exit 2;}
[ -s "$INPUT_FILE" ] || { (usage ; echo -e "ERROR : Empty file error for $INPUT_FILE \n") >&2 ; exit 2;}
[[ "$INPUT_FILE" == *.fastq ]] || { (usage ; echo -e "ERROR : '.fastq' file extention expected $INPUT_FILE \n") >&2 ; exit 2;}

# testing the second parameter : the number from 1 to 9
echo "$LINES_NUM" | grep -w -e "^[1-9]$" || { (usage ; echo -e "ERROR : second parameter should be an integer from 1 to 9, '$LINES_NUM' provided \n") >&2; exit 2;} #TODO remove the number printed to stdout !!!
input_file_lines_num=$(cat "$INPUT_FILE" | wc -l | tr -d '[:space:]') 
(( "$input_file_lines_num" >= "$LINES_NUM" )) || { (usage ;  echo -e "ERROR : fastq file is too short ($input_file_lines_num lines) but  $LINES_NUM lines extraction asked \n") >&2; exit 2;}

# testing the  third parameter : the output directory
[ -e "$OUTPUT_DIR" ] || { 
	      mkdir -p "$OUTPUT_DIR" || { (usage ; echo -e "ERROR : directory $OUTPUT_DIR does not exist and can not create it\n") >&2; exit 2;} && echo -e "Directory $OUTPUT_DIR missing. Created automatically ! \n"
          } 
[ -w "$OUTPUT_DIR" ] || { (usage ; echo -e "ERROR : write permission error in directory $OUTPUT_DIR\n") >&2; exit 2;}



######################################
# CORE CODE
input_file_basename=$(basename "$INPUT_FILE")
head -n $LINES_NUM "$INPUT_FILE" > "${OUTPUT_DIR}/${input_file_basename}.head" || { (echo -e "ERROR : could not extract the first $LINES_NUM from $INPUT_FILE and write them to ${OUTPUT_DIR}/${input_file_basename}.head file \n") >&2; exit 3;}
tail -n $LINES_NUM "$INPUT_FILE" > "${OUTPUT_DIR}/${input_file_basename}.tail" || { (echo -e "ERROR : could not extract the last $LINES_NUM from $INPUT_FILE and write them to ${OUTPUT_DIR}/${input_file_basename}.head file \n") >&2; exit 3;}


######################################
# END
echo -e "file processed with success !\n"
exit 0

